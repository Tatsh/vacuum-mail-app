interface ObjCGlobal {
  ['import'](name: string): void;
  bindFunction(name: string, args: any[]);
  block();
  castObjectToRef();
  castRefToObject();
  deepUnwrap<I = any, R = any>(x: I): R;
  dict();
  interactWithUser: boolean;
  registerSubclass();
  super();
  unwrap<I = any, R = any>(x: I): R;
  wrap<I = any, R = any>(x: I): R;
}
declare var ObjC: ObjCGlobal;

interface ObjectSpecifierGlobal {
  classOf(x: any): any;
}
declare var ObjectSpecifier: ObjectSpecifierGlobal;

declare namespace Automation {
  function getDisplayString(obj: any, print?: boolean);
  function initializeGlobalObject();
}

declare namespace Progress {
  var additionalDescription: string | void;
  var completedUnitCount: number | void;
  var description: string | void;
  var totalUnitCount: number | void;
}

declare function Library<T = any>(name: string): T;

declare function delay(n: seconds): void;

interface PathObject {}
declare function Path(s: string): PathObject;

declare namespace console {
  function log(...args: any): void;
}

interface BridgedObject<T> {
  getProperty(key: string);
  isNil(): boolean;
  js: T;
  setProperty<U = any>(key: string, value: U);
}

interface IdType {}

interface NSError extends BridgedObject<IdType> {
  code: string;
  domain: NSString;
}

interface NSNumber extends BridgedObject<number> {}

interface NSString extends BridgedObject<string> {
  compareOptions(s: NSString | string, n: number): number;
  initWithDataEncoding(d: NSData, encoding: number): NSString;
  writeToFileAtomically(
    filename: NSString | string,
    atomically: boolean
  ): boolean;
}

interface NSDictionary<KeyType = NSString, ValueType = any>
  extends BridgedObject<{}> {
  objectForKey<R = ValueType>(s: KeyType): R;
}

interface NSArray<T = any> extends BridgedObject<Array> {
  containsObject(obj: T): boolean;
}

interface NSFileManager extends BridgedObject<IdType> {
  attributesOfItemAtPathError(
    path: NSString | string,
    error: NSError | void
  ): NSDictionary;
  contentsOfDirectoryAtPathError(
    path: NSString | string,
    error: NSError | void
  ): NSArray<NSString>;
  fileExistsAtPath(path: string | NSString): boolean;
}

interface Application {
  activate(): void;
  commandsOfClass(): any;
  displayDialog(
    message: string,
    args?: {
      withTitle?: string;
      defaultAnswer?: string;
    }
  );
  displayNameForCommand(): string | void;
  displayNameForElementOfClass(): string | void;
  displayNameForPropertyOfClass(cls?: any): string | void;
  elementsOfClass(): any[];
  frontmost(): boolean;
  id(): any;
  includeStandardAdditions: boolean;
  launch(): void;
  name(): string;
  parentOfClass(): any;
  propertiesOfClass(): any[];
  quit(): void;
  running(): boolean;
  say(s: string): void;
  strictCommandScope: boolean;
  strictParameterType: boolean;
  strictPropertyScope: boolean;
  version(): string;
}

interface NSData extends BridgedObject<IdType> {}

interface NSFileHandle extends BridgedObject<IdType> {
  readonly closeFile: void;
  readonly readDataToEndOfFile: NSData;
}

interface NSTask extends BridgedObject<IdType> {
  arguments: (NSString | string)[];
  launchPath: NSString | string;
  readonly init: NSTask;
  readonly launch: void;
  readonly terminationStatus: number;
  readonly waitUntilExit: void;
  standardError?: NSPipe;
  standardOutput?: NSPipe;
}

interface NSPipe extends BridgedObject<IdType> {
  readonly fileHandleForReading: NSFileHandle;
}

interface RefType<T> {
  [index: number]: T;
}
declare function Ref<T = any>(val: any): RefType<T>;

declare function Application(x: string | number): Application;

/**
 * Alias function to `ObjC.wrap()`.
 */
declare function $<I = any, R = any>(inValue: I): R;

/**
 * Namespace where all `ObjC.import()` imported functions are placed.
 */
declare namespace $ {
  // stdlib
  function _Exit(code: number): void;
  function abort(): void;
  function arc4random(): number;
  function arc4random_addrandom(dat: Ref<string>, datlen: number): void;
  function arc4random_buf<T = any>(buf: RefType<T>, size: number): void;
  function arc4random_stir(): void;
  function arc4random_uniform(max: number): number;
  function atexit(func: any): number;
  function atof(x: string): number;
  function atoi(x: string): number;
  function atoll(x: string): number;
  function exit(code: number): void;
  function getenv(name: string): string | void;
  function putenv(s: string): number;
  function rand(): number;
  function setenv(name: string, value: string, overwrite: number): number;
  function srand(seed: number): void;
  function strtod(x: string, se: number | void): number;
  function strtof(s: string, se: number | void): number;
  function strtold(x: string, se: number | void): number;
  function system(cmd: string): number;
  function unsetenv(name: string): number;

  // Cocoa
  function NSBeep(): void;

  // Generic constants
  const NSNumericSearch: number;
  const NSOrderedDescending: number;

  // NSArray
  namespace NSArray {
    const alloc: NSArray;
  }

  // NSFile*
  var NSFileSize: NSString;
  namespace NSFileManager {
    const defaultManager: NSFileManager;
  }

  // NSPipe
  namespace NSPipe {
    const alloc: NSPipe;
    const pipe: NSPipe;
  }

  // NSString
  namespace NSString {
    const alloc: NSString;
  }
  const NSUTF8StringEncoding: number;

  // NSTask
  namespace NSTask {
    const alloc: NSTask;
    function launchedTaskWithLaunchPathArguments(
      path: string | NSString,
      args: (string | NSString)[]
    ): NSTask;
  }
}
