#!/usr/bin/osascript -l JavaScript
"use strict";
ObjC.import('Foundation');
ObjC.import('stdlib');
const getOutput = (launchPath, args, trim = false) => {
    const pipe1 = $.NSPipe.pipe;
    const pipe2 = $.NSPipe.pipe;
    const stdout = pipe1.fileHandleForReading;
    const stderr = pipe2.fileHandleForReading;
    const task = $.NSTask.alloc.init;
    task.launchPath = launchPath;
    task.arguments = args;
    task.standardOutput = pipe1;
    task.standardError = pipe2;
    task.launch;
    task.waitUntilExit;
    const stdoutData = stdout.readDataToEndOfFile;
    const stderrData = stderr.readDataToEndOfFile;
    stdout.closeFile;
    stderr.closeFile;
    const ret = [task.terminationStatus];
    for (const data of [stdoutData, stderrData]) {
        const d = ObjC.unwrap($.NSString.alloc.initWithDataEncoding(data, $.NSUTF8StringEncoding));
        ret.push(trim ? d.trim() : d);
    }
    return ret;
};
class FileManager {
    constructor() {
        this.fm = $.NSFileManager.defaultManager;
    }
    getSize(path) {
        return ObjC.unwrap(this.fm
            .attributesOfItemAtPathError(path, void 0)
            .objectForKey($.NSFileSize));
    }
    *getDirectoryContents(path) {
        const ret = [];
        for (const x of ObjC.unwrap(this.fm.contentsOfDirectoryAtPathError(path, void 0))) {
            yield ObjC.unwrap(x);
        }
    }
    fileOrDirectoryExists(path) {
        return this.fm.fileExistsAtPath(path);
    }
}
class Mail {
    constructor() {
        this.app = Application('Mail');
        this.fm = new FileManager();
    }
    getEnvelopeIndexPath() {
        if (getOutput('/usr/bin/sw_vers', ['-productVersion'], true)[0] !== 0) {
            throw new Error('sws_vers command failed');
        }
        const mailDataPath = (() => {
            for (const mailVersion of [8, 7, 6, 5, 3, 2]) {
                const testPath = `${$.getenv('HOME')}/Library/Mail/V${mailVersion}/MailData`;
                if (this.fm.fileOrDirectoryExists(testPath)) {
                    return testPath;
                }
            }
            return void 0;
        })();
        if (!mailDataPath) {
            throw new Error('Unknown mail data path');
        }
        for (const fn of this.fm.getDirectoryContents(mailDataPath)) {
            if (!/Envelope Index$/.test(fn)) {
                continue;
            }
            return `${mailDataPath}/${fn}`;
        }
        throw new Error('Failed to find file matching /Envelope Index$/');
    }
    vacuumIndex() {
        const path = this.getEnvelopeIndexPath();
        const sizeBefore = this.fm.getSize(path);
        const task = $.NSTask.launchedTaskWithLaunchPathArguments('/usr/bin/sqlite3', [path, 'vacuum']);
        task.waitUntilExit;
        if (task.terminationStatus !== 0) {
            throw new Error('Failed to vacuum; sqlite3 command failed');
        }
        return [sizeBefore, this.fm.getSize(path)];
    }
}
const main = () => {
    const mail = new Mail();
    const wasRunning = mail.app.running();
    if (wasRunning) {
        getOutput('/usr/bin/killall', ['Mail']);
    }
    const [sizeBefore, sizeAfter] = (() => {
        try {
            return mail.vacuumIndex();
        }
        catch (e) {
            console.log(e.toString());
        }
        return [void 0, void 0];
    })();
    if (typeof sizeBefore === 'undefined' || typeof sizeAfter === 'undefined') {
        return 1;
    }
    console.log(`Mail index before: ${sizeBefore}, after: ${sizeAfter} bytes, ${Math.abs((sizeAfter / sizeBefore - 1) * 100).toFixed(3)}% ${sizeAfter === sizeBefore
        ? 'change'
        : sizeAfter > sizeBefore
            ? 'increase'
            : 'decrease'}`);
    if (wasRunning) {
        mail.app.activate();
    }
    return 0;
};
$.exit(main());
